import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import csv
import sys

caps = '9-5','10','10-5','11','11-5','12','12-5','13','13-5','14','14-5','15'
nums = 1,2,3,4,5
types = 'profiler','nowrapper'
powers = [[]]
times = [[]]
powersums=[[[]]]
i=0
for cap in caps:
    #for type in types:
    times.append([])
    powers.append([])
    powersums.append([])


    n=0

    for num in nums:
        with open(sys.argv[1]+'/cap_'+cap+'/'+types[0]+'-'+str(num)+'/energy.box.dat','r') as csvfile:
            plots = csv.reader(csvfile, delimiter=' ')
            r=0
            for row in plots:
                if r>1:
                    k=r-2
                    if float(row[1])>0:
                        if k > len(powersums[i])-1:
                            powersums[i].append([])
                        elif len(powersums[i][k]) != 2:
                            powersums[i][k].append(0)
                            powersums[i][k].append(0)
                        else:
                            e = float(row[1])
                            powersums[i][k][0] += e/0.1
                            powersums[i][k][1] += 1
                        try:
                            if k > len(powersums[i])-1:
                                powersums[i].append([])
                            elif len(powersums[i][k]) != 2:
                                powersums[i][k].append(0)
                                powersums[i][k].append(0)
                            e = float(row[1])
                            powersums[i][k][0] += e/0.1
                            powersums[i][k][1] += 1
                        except:
                            print(str(cap)+' num: '+str(num))
                            print('k'+str(k))
                            print(len(powersums[i]))
                r+=1
        n+=1
    time=0
    for sum in powersums[i]:
        powers[i].append(sum[0]/sum[1])
        time+=0.1
        times[i].append(time)
    i+=1




x=0
lines=[]
while x < i:
    lines.append(plt.plot(times[x],powers[x], label=caps[x]))
    x+=1
plt.legend(loc='best')
plt.xlabel('Time (s)')
plt.ylabel('Power (W)')
plt.title('power vs. time for '+sys.argv[1])
plt.show(lines)
