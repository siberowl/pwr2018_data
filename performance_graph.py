import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import csv
import sys

benchmark = sys.argv[1]
proc = sys.argv[2]

times = [[]]
caps = [[]]
pows = [[]]

types = 'profiler','nowrapper','noprofiler'
t=0
for type in types:
    times.append([])
    caps.append([])
    pows.append([])
    with open('database.csv','r') as csvfile:
            plots = csv.reader(csvfile, delimiter=',')
            for row in plots:
                if row[0]==benchmark and row[2]==proc and row[1] == type:
                    if row[1] != 'noprofiler':
                        pows[t].append(float(row[5]))
                    times[t].append(float(row[8]))
                    caps[t].append(float(row[3]))
    t+=1
fig, ax1 = plt.subplots()
ax2 = ax1.twinx()
ax1.plot(caps[0],times[0],label='profiler')
ax1.plot(caps[1],times[1],label='no wrapper')
ax1.plot(caps[2],times[2],label='no profiler')

ax2.scatter(caps[0],pows[0],label='profiler average power')
ax2.scatter(caps[1],pows[1],label='no wrapper average power')

plt.xlabel('Power Cap (W)')
ax1.set_ylabel('Time (s)')
ax2.set_ylabel('Ave. Power (W)')
ax1.legend(loc='center right')
ax2.legend(loc='lower right')
plt.title(benchmark+' '+proc+' processes')
plt.show()
