import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import csv
import sys

sets = ['lulesh','output_fixed/lulesh_size52_proc1',1],['miniFE','output_fixed/miniFE_proc2',2],['miniFE','output_fixed/miniFE_proc1',1],['graph500','output_fixed/graph500_proc2',2],['graph500','output_fixed/graph500_proc1',1]



type = 'profiler'
s=0
for set in sets:
    caps=[]
    stds = []
    with open('database.csv','r') as csvfile:
            plots = csv.reader(csvfile, delimiter=',')
            r=0
            for row in plots:
                if row[0]==set[0] and row[2]==str(set[2]) and row[1]==type:
                    caps.append(float(row[3]))
                    stds.append(float(row[7]))
                    r+=1

    plt.plot(caps,stds,label='profiler_'+set[0]+'_'+str(set[2]))
    s+=1

plt.xlabel('Power Cap (W)')
plt.ylabel('Standard Deviation')
plt.legend(loc='lower right')
plt.title('energy standard deviation vs. powercap')
plt.show()
