import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import csv
import sys

sets = ['lulesh','output_fixed/lulesh_size52_proc1',1],['miniFE','output_fixed/miniFE_proc2',2],['miniFE','output_fixed/miniFE_proc1',1],['graph500','output_fixed/graph500_proc2',2],['graph500','output_fixed/graph500_proc1',1]


fig, ax1 = plt.subplots()
ax2 = ax1.twinx()

type = 'profiler'
s=0

print(sys.argv[1])
print(sys.argv[2])

for set in sets:
    if sys.argv[1]==set[0] and sys.argv[2]==str(set[2]):
        print("matched")
        caps=[]
        energies = []
        times = []
        with open('database.csv','r') as csvfile:
                plots = csv.reader(csvfile, delimiter=',')
                r=0
                for row in plots:
                    if row[0]==set[0] and row[2]==str(set[2]) and row[1]==type:
                        caps.append(float(row[3]))
                        energies.append(float(row[4]))
                        times.append(float(row[8]))
                        r+=1

        ax1.plot(caps,times,label='profiler_'+set[0]+'_'+str(set[2]))
        ax2.scatter(caps,energies,label='profiler_'+set[0]+'_'+str(set[2]))
    s+=1


plt.xlabel('Power Cap (W)')
ax1.set_ylabel('Time (s)')
ax2.set_ylabel('Total Energy (J)')
ax1.legend(loc='lower right')
ax1.legend(loc='lower left')
plt.title('time and tot ener. vs. powercap')
plt.show()
