import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import csv

energydatname = 'energy.box.dat'
timedatname = 'time.dat'

f = open("database.csv", "w+")
f.write('benchmark,type,processes #,powercap,total energy,average power,avepow/cap,energy standard deviation,average time,time standard deviation\n')
benchmarks = ['lulesh','output_fixed/lulesh_size52_proc1',1],['miniFE','output_fixed/miniFE_proc2',2],['miniFE','output_fixed/miniFE_proc1',1],['graph500','output_fixed/graph500_proc2',2],['graph500','output_fixed/graph500_proc1',1]
caps = '9-5','10','10-5','11','11-5','12','12-5','13','13-5','14','14-5','15'
nums = 1,2,3,4,5
types = 'profiler','nowrapper','noprofiler'

for benchmark in benchmarks:
    for cap in caps:
        for type in types:
            powercap = 0

            totenergy = 0

            powsum = 0
            aveenergy = 0
            avepow = 0

            percap = 0

            sumdeviation = 0
            energystd = 0

            timesum = 0
            avetime = 0
            sumtimedeviation = 0
            timestd = 0
            timecounter=0

            r1=0
            run=0
            while run<2:
                for num in nums:
                    path=benchmark[1]+'/cap_'+cap+'/'+type+'-'+str(num)
                    try:
                        if run==0:
                            #powercap
                            if len(cap.split('-')) == 2:
                                powercap = float(cap.split('-')[0])+float(cap.split('-')[1])/10
                            else:
                                powercap = float(cap.split('-')[0])

                            if type != 'noprofiler':
                                with open(path+'/'+energydatname,'r') as csvfile:
                                        plots = csv.reader(csvfile, delimiter=' ')
                                        r=0
                                        prevt=0
                                        for row in plots:
                                            if r==1:
                                                prevt=float(row[0])
                                            if r>1:
                                                if abs(float(row[0])-prevt) > 104.0:
                                                    print(str(float(row[0]))+' and '+str(prevt))
                                                    print('diff is '+str(abs(float(row[0])-prevt)))
                                                    print(path)
                                                    print('at line '+str(r+1))
                                                
                                                prevt=float(row[0])

                                                try:
                                                    if float(row[1])>0:
                                                        powsum+=float(row[1])
                                                        totenergy+=float(row[1])
                                                except:
                                                    print(row)
                                                    print(path)

                                            r+=1
                                            r1+=1


                            #time
                            with open(path+'/'+timedatname,'r') as csvfile:
                                    plots = csv.reader(csvfile, delimiter='	')
                                    for row in plots:
                                        if len(row)>1:
                                            if row[0]=="real":
                                                curr=row[1]
                                                timesum += float(curr.split('m')[0])*60+float( curr.split('m')[1].split('s')[0] )
                                                timecounter+=1
                        else:
                            if type != 'noprofiler':
                                try:
                                    aveenergy=float(powsum/r1)
                                except:
                                    print(benchmark[1]+' '+type+' '+str(cap)+' '+str(num))
                                avepow = aveenergy/0.1
                                percap=avepow/powercap
                            avetime = timesum/timecounter

                            if type != 'noprofiler':
                                with open(path+'/'+energydatname,'r') as csvfile:
                                        plots = csv.reader(csvfile, delimiter=' ')
                                        r=0
                                        for row in plots:
                                            if r>1:
                                                if float(row[1])>0:
                                                    sumdeviation+=(aveenergy-float(row[1])) ** 2
                                            r+=1
                            with open(path+'/'+timedatname,'r') as csvfile:
                                    plots = csv.reader(csvfile, delimiter='	')
                                    for row in plots:
                                        if len(row)>1:
                                            if row[0]=="real":
                                                curr=row[1]
                                                currtime = float(curr.split('m')[0])*60+float( curr.split('m')[1].split('s')[0] )
                                                sumtimedeviation+=(avetime-currtime) ** 2

                    except FileNotFoundError:
                        break


                run+=1
            if type != 'noprofiler':
                energystd = (sumdeviation/r1) ** 0.5
            timestd = (sumtimedeviation/len(nums)) ** 0.5
            f.write(benchmark[0]+','+type+','+str(benchmark[2])+','+str(powercap)+','+str(totenergy)+','+str(avepow)+','+str(percap)+','+str(energystd)+','+str(avetime)+','+str(timestd)+'\n')
