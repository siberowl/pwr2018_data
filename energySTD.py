import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import csv
import sys

sets = ['lulesh','output_fixed/lulesh_size52_proc1',1],['miniFE','output_fixed/miniFE_proc2',2],['miniFE','output_fixed/miniFE_proc1',1],['graph500','output_fixed/graph500_proc2',2],['graph500','output_fixed/graph500_proc1',1]

caps=[[]]
stds = [[]]

types = 'profiler','nowrapper'
t=0

for type in types:
    caps.append([])
    stds.append([])
    s=0
    while s < len(sets):
        with open('database.csv','r') as csvfile:
                plots = csv.reader(csvfile, delimiter=',')
                r=0
                for row in plots:
                    if row[0]==sets[s][0] and row[2]==str(sets[s][2]):
                        if row[1] == type:
                            if s==0:
                                caps[t].append(float(row[3]))
                                stds[t].append(float(row[6]))
                            else:
                                stds[t][r]+=float(row[6])

                            r+=1
        s+=1
    c=0
    for cap in caps[t]:
        stds[t][c]=stds[t][c]/len(sets)
        c+=1

    t+=1
fig, ax1 = plt.subplots()
ax1.plot(caps[0],stds[0],label='profiler')
ax1.plot(caps[1],stds[1],label='no wrapper')

plt.xlabel('Power Cap (W)')
ax1.set_ylabel('Standard Deviation')
ax1.legend(loc='lower right')
plt.title('energy standard deviation vs. powercap')
plt.show()
