import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import csv
import sys

benchmark = sys.argv[1]
proc = sys.argv[2]

times = [[]]
caps = [[]]
pows = [[]]

types = 'profiler','nowrapper'
t=0
for type in types:
    times.append([])
    caps.append([])
    pows.append([])
    with open('database.csv','r') as csvfile:
            plots = csv.reader(csvfile, delimiter=',')
            for row in plots:
                if row[0]==benchmark and row[2]==proc and row[1] == type:
                    pows[t].append(float(row[5]))
                    times[t].append(float(row[8]))
    t+=1
plt.plot(pows[0],times[0],label='profiler')
plt.plot(pows[1],times[1],label='no wrapper')

plt.xlabel('Average Power (W)')
plt.ylabel('Time (s)')
plt.legend(loc='best')
plt.title(benchmark+' '+proc+' processes')
plt.show()
